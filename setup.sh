#!/usr/bin/env bash

# exit on any error
set -e

# install dependencies
apt update
apt install -y python-influxdb python-serial

# install pv-inverter-monitor script, env and service files
curl -L https://gitlab.com/mccleanp/pv-inverter-monitor/-/raw/master/pv-inverter-monitor.py -o /usr/local/bin/pv-inverter-monitor.py
chmod +x /usr/local/bin/pv-inverter-monitor.py
curl -L https://gitlab.com/mccleanp/pv-inverter-monitor/-/raw/master/pv-inverter-monitor.env -o /usr/local/etc/pv-inverter-monitor.env
curl -L https://gitlab.com/mccleanp/pv-inverter-monitor/-/raw/master/pv-inverter-monitor.service -o /etc/systemd/system/pv-inverter-monitor.service
curl -L https://gitlab.com/mccleanp/pv-inverter-monitor/-/raw/master/pv-inverter-monitor.timer -o /etc/systemd/system/pv-inverter-monitor.timer

# configure
editor /usr/local/etc/pv-inverter-monitor.env </dev/tty

# start
systemctl enable pv-inverter-monitor.timer
systemctl start pv-inverter-monitor.timer

echo "PV Inverter Monitor installed successfully"
