#!/usr/bin/env python

from __future__ import print_function
import sys
import struct
import json
import os
from collections import namedtuple
from datetime import datetime
from serial import Serial, EIGHTBITS, PARITY_NONE, STOPBITS_ONE
from influxdb import InfluxDBClient

START=0x55aa
INIT=0x004
REQUEST_SERIAL_NUMBER=0x0000
CONFIRM_SERIAL_NUMBER=0x0001
RESET=0x0301
READ_MODEL_VERSION=0x0103
READ_STATUS_DATA_FORMAT=0x0100
READ_STATUS_DATA=0x0102

fields = [None] * 0x40
fields[0x00] = ('temperature', 0.1)
fields[0x01] = ('voltagePV', 0.1)
fields[0x04] = ('currentPV', 0.1)
fields[0x09] = ('operatingHours', 65536)
fields[0x0a] = ('operatingHours', 1)
fields[0x11] = ('energyToday', 0.01)
fields[0x31] = ('currentAC', 0.1)
fields[0x32] = ('voltageAC', 0.1)
fields[0x33] = ('frequencyAC', 0.01)
fields[0x34] = ('powerAC', 1)
fields[0x35] = ('energyTotal', 6553.6)
fields[0x36] = ('energyTotal', 0.1)

InverterInfo = namedtuple('InverterInfo', 'deviceType, vaRating, fwVersion, modelName, manufacturer, serialNumber')
InverterValues = namedtuple('InverterValues', 'temperature, voltagePV, currentPV, energyTotal, energyToday, operatingHours, currentAC, voltageAC, frequencyAC, powerAC')
InverterValues.__new__.__defaults__ = (None,) * len(InverterValues._fields)
        
ttyPort = sys.argv[1] if len(sys.argv) > 1 else os.environ['INVERTER_PORT']

serialPort = Serial(ttyPort, 9600, timeout=1, bytesize=EIGHTBITS, parity=PARITY_NONE, stopbits=STOPBITS_ONE, xonxoff=False,rtscts=False, dsrdtr=False)

influx = InfluxDBClient(
        os.environ['INFLUXDB_HOST'],
        os.environ.get('INFLUXDB_PORT'),
        os.environ.get('INFLUXDB_USER'),
        os.environ.get('INFLUXDB_PASS'),
        os.environ['INFLUXDB_DATABASE']);

def transmit(buf):
    print('TX ({}) -> {}'.format(len(buf), ' '.join(format(x, '02x') for x in buf)), file=sys.stderr)
    serialPort.write(buf)

def receive():
    buf = bytearray(serialPort.read(100))
    print('RX ({}) <- {}'.format(len(buf), ' '.join(format(x, '02x') for x in buf)), file=sys.stderr)
    return buf


def to_bytes(uint):
    return [ (uint >> 8) & 0xff, uint & 0xff ]

def build_request(command, start=START, source=0, dest=0, data=None):
    packet = bytearray(struct.pack('>HHHH', start, source, dest, command))
    data = data or []
    packet.extend(struct.pack('B', len(data)))
    packet.extend(data)
    packet.extend(struct.pack('>H', sum(packet)))
    return packet

def parse_response(packet):
    while len(packet) >= 2:
        (start,) = struct.unpack_from('>H', packet)
        if (start == START):
            break
        del packet[0]

    (source, dest, command, length) = struct.unpack_from('>HHHB', packet[2:])
    data = packet[9:9+length]
    (checksum,) = struct.unpack_from('>H', packet[9+length:])
    if sum(packet[0:9+length]) != checksum:
        raise Exception('Bad checksum')
    return (source, dest, command, data)


def reset():
    transmit(build_request(RESET))
    parse_response(receive())

def init():
    print("init", file=sys.stderr)
    transmit(build_request(INIT))

def request_serial_number():
    print("request_serial_number", file=sys.stderr)
    transmit(build_request(REQUEST_SERIAL_NUMBER))
    (source, dest, command, data) = parse_response(receive())
    # assume all details are correct
    return str(data)

def confirm_serial_number(serial):
    print("confirm_serial_number", file=sys.stderr)
    data = bytearray(serial)
    transmit(build_request(CONFIRM_SERIAL_NUMBER, data=data))
    (source, dest, command, data) = parse_response(receive())
    return source

def read_inverter_info(dest=1):
    print("read_inverter_info", file=sys.stderr)
    transmit(build_request(READ_MODEL_VERSION, dest=dest))
    (source, dest, command, data) = parse_response(receive())
    return InverterInfo._make(map(lambda s: s.strip(), struct.unpack_from('>1s6s5s16s16s10s', data)))

def read_status_data_format(dest=1):
    print("read_status_data_format", file=sys.stderr)
    transmit(build_request(READ_STATUS_DATA_FORMAT, dest=dest))
    (source, dest, command, data) = parse_response(receive())
    return struct.unpack_from(str(len(data))+'B', data)

def read_status_data(dest=1):
    print("read_status_data", file=sys.stderr)
    transmit(build_request(READ_STATUS_DATA, dest=dest))
    (source, dest, command, data) = parse_response(receive())
    return struct.unpack_from('>'+str(len(data)/2)+'H', data)

def read_inverter_values(dest=1):
    values = {}
    data_format = read_status_data_format(address)
    status = read_status_data(address)
    for index, value in enumerate(status):
        field = fields[data_format[index]]
        if field is not None:
            (name, scale) = field
            values[name] = values.get(name,0) + value * scale
    return InverterValues(**values)

init()

serial = request_serial_number()
address = confirm_serial_number(serial)

info = read_inverter_info(address)
values = read_inverter_values(address)

points = [ {
    'measurement': os.environ['INFLUXDB_MEASUREMENT'],
    'time': datetime.now().isoformat(),
    'tags': InverterInfo._asdict(info),
    'fields': InverterValues._asdict(values)
} ]

#print(json.dumps(points))
influx.write_points(points)

serialPort.close()
