#!/usr/bin/env python

# This is a simple protocol simulator. It must be given arguments for the tty port
# and a file containing protcol messages.

import sys
from serial import Serial, EIGHTBITS, PARITY_NONE, STOPBITS_ONE

ttyPath = sys.argv[1]
messagesPath = sys.argv[2]

messages = dict()
with open(messagesPath) as f:
    lines = f.read().splitlines()
    for req, res in zip(lines[::2], lines[1::2]):
        messages[str(bytearray.fromhex(req))] = str(bytearray.fromhex(res))

ser = Serial(ttyPath, 9600, timeout=0, bytesize=EIGHTBITS, parity=PARITY_NONE, stopbits=STOPBITS_ONE, xonxoff=False,rtscts=False, dsrdtr=False)

def transmit(buf):
    print('TX ({}): {}'.format(len(buf), ' '.join(format(x, '02x') for x in bytearray(buf))))
    ser.write(buf)

def receive():
    buf = []
    while len(buf) == 0:
        buf = ser.read(100)
    print('RX ({}): {}'.format(len(buf), ' '.join(format(x, '02x') for x in bytearray(buf))))
    return buf

while True:
    req = receive()
    res = messages.get(req)
    if res is not None:
        transmit(res)

ser.close()
